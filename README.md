---
### ![goup.png](http://html-applications.bitbucket.io/images/goup.png) [Return to HTML Application page](http://html-applications.bitbucket.io) ###
---
## ![ado-find.png](http://html-applications.bitbucket.io/images/ado-find.png)  [Active Directory Objects Finder](http://html-applications.bitbucket.io/ado-find/readme.html) ##
Easy find any objects of Active Directory

Легкий поиск и детальный просмотр свойств любых объектов Active Directory.