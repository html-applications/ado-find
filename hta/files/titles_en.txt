accountExpires	Account Expires (use same date format as server)
altRecipient	Forward To
authOrig	Accept Messages From
c	Country 2 Digit Code - eg. US
co	Country 
company	Company
countryCode	Country code -eg. for US country code is 840
deletedItemFlags	Do not permanaently delete messages until the store has been backed up
delivContLength	Receiving Message Size (KB)
deliverAndRedirect	Deliver and Redirect
department	Department
description	Description
displayName	Display Name
displayNamePrintable	Simple Display Name
facsimileTelephoneNumber	Fax
garbageCollPeriod	keep deleted items for (days)
givenName	First Name
homeDirectory	Home Folder
homeDrive	Home Drive
homePhone	Home
info	Notes
initials	Middle Name / Initials
ipPhone	IP Phone
l	City
mail	Email
mailNickName	Mail Alias
manager	Manager
mDBOverHardQuotaLimit	Prohibit Send and receive at (KB)
mDBOverQuotaLimit	Prohibit Send at (KB)
mDBStorageQuota	Issue Warning at (KB)
mDBuseDefaults	Use mailbox store defaults
memberOf	Group
mobile	Mobile
msExchHideFromAddressLists	Hide from Exchange address lists
msExchOmaAdminWirelessEnable	Outlook Mobile Access 
msExchRecipLimit	Reciepient Limits
msExchRequireAuthToSendTo	Accept messages from Authenticated Users only
name/cn	Full  Name
pager	Pager
password	Password
physicalDeliveryOfficeName	Office
postalCode	Zip/Postal Code
postOfficeBox	PO Box
profilePath	Profile Path
protocolSettings	Outlook Web Access 
publicDelegates	Send on Behalf
sAMAccountName	Logon Name (Pre Windows 2000)
scriptPath	Login Script
sn	Last Name
st	State/Province
streetAddress	Street
submissionContLength	Sending Message Size (KB)
telephoneNumber	Telephone Number
title	Title
tsAllowLogon	Allow Terminal Server Logon
tsBrokenTimeOutSettings	When session limit reached or connection broken
tsDeviceClientDefaultPrinter	Default to main client printer
tsDeviceClientDrives	Connect client drive at logon
tsDeviceClientPrinters	Connect client printer at logon
tsHomeDir	Terminal Services Home Directory 
tsHomeDirDrive	Terminal Services Home Drive
tsInheritInitialProgram	Start the following program at logon
tsIntialProgram	Starting Program file name
tsProfilePath	Terminal Services Profile Path
tsReConnectSettings	Allow reconnection
tsShadowSettings	Remote Control
tsTimeOutSettingsConnections	Active Session limit
tsTimeOutSettingsDisConnections	End disconnected session
tsTimeOutSettingsIdle	Idle session limit
tsWorkingDir	Start in
unauthOrig	Reject Messages From
userAccountControl	User Account Control 
userPrincipalName	Logon Name
userWorkstations	Log on to
wWWHomePage	Web Page
