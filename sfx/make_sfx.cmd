@echo off
CD /D "%~dp0"

rem https://www.7-zip.org/
set Sz="c:\Program Files (x86)\7-Zip\7z.exe"
if not exist %Sz% set Sz=g:\_tc\Utils\7Zip\7z.exe
if not exist %Sz% echo 7zip not exist! & pause & exit /b

for /f "tokens=2 delims==" %%a in ('findstr "version=" ..\hta\ADOFinder.hta') do set ver=%%~a
echo ADOFinder.hta version %ver%
rem http://blog.dixo.net/downloads/stampver/
stampver.exe -f"%ver%.0" -p"%ver%.0" ADOFinder.sfx

pushd "..\hta"
	%Sz% a %~dp0archive.7z .\
popd

if exist ..\ADOFinder.exe del /q ..\ADOFinder.exe

copy /b ADOFinder.sfx + config.txt + archive.7z ..\ADOFinder.exe

del /q archive.7z

pause